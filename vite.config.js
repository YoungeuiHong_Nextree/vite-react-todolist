import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default ({
  plugins: [react()]

})

const path = require('path')

module.exports = defineConfig({
    build: {
        lib: {
            // 번들링 진입점은 main.ts, 파일 이름은 ToDoList로
            entry: path.resolve(__dirname, 'src/main.tsx'),
            name: 'ToDoList',
            fileName: (format) => `ToDoList.${format}.js`
        },
/*        watch: {
            // 파일 변경 시 다시 빌드되도록 Rollup Watcher 활성화
            include: 'src*//**'
        }*/
    }
})

