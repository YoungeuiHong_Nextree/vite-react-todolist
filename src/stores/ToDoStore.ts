import { observable } from 'mobx';

const todos = observable([
    {id: 1, title: 'todo-1', status: 'finish'},
    {id: 2, title: 'todo-2', status: 'doing'}
])

export default ToDoStore
