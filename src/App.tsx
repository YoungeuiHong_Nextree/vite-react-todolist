import React from 'react'
import ToDoList from './views/ToDoList'

function App() {

  return (
    <div>
        <h3>to-do list</h3>
        <ToDoList/>
    </div>
  );
}

export default App
