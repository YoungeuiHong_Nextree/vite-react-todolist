import React, { useState } from "react";
import {ListItem, ListItemButton, ListItemIcon, ListItemText, Checkbox } from '@mui/material';

function ToDoItem(props) {

    const [todo, setTodo] = useState(props);

    return (
        <ListItem>
            <ListItemButton>
                <ListItemIcon>
                    <Checkbox/>
                </ListItemIcon>
                <ListItemText id={todo.id} primary={todo.title}/>
            </ListItemButton>
        </ListItem>
    );
}

export default ToDoItem
