import ToDoItem from './ToDoItem';
import React, { useState, useEffect } from "react";
import { List } from '@mui/material';

function ToDoList() {

    const [todos, setTodos] = useState([{id: 1, title: 'todo-1', status: 'finish'},
                                        {id: 2, title: 'todo-2', status: 'doing'}]);

    return(
        <List>
            {todos.map((todo) => {
                return <ToDoItem key={todo.id} title={todo.title}/>
            })}
        </List>
    );
}

export default ToDoList
